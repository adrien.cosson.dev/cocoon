import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
// import DashboardView from '../views/dashboard/DashboardView.vue'

// import {useUserStore} from "../stores/user";

// const userStore = useUserStore()

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/Vente',
      name: 'sale',
      component: () => import('../views/SaleView.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/:address/:type-:id',
      name: 'property',
      component: () => import('../views/PropertyView.vue')
    },
// DASHBOARD
    {
      path: '/Dashboard',
      name: 'dashboard',
      component: () => import('../views/DashboardView.vue')
      ,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/Profil',
      name: 'profile',
      component: () => import('../views/DashboardView.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/Mes-annonces',
      name: 'myAds',
      component: () => import('../views/DashboardView.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/Dashboard/annonces',
      name: 'ads',
      component: () => import('../views/DashboardView.vue')
    },
    {
      path: '/Dashboard/types',
      name: 'types',
      component: () => import('../views/DashboardView.vue')
    },
    {
      path: '/Dashboard/categories',
      name: 'categories',
      component: () => import('../views/DashboardView.vue')
    },
    {
      path: '/Dashboard/statuts',
      name: 'status',
      component: () => import('../views/DashboardView.vue')
    },
    {
      path: '/Dashboard/roles',
      name: 'roles',
      component: () => import('../views/DashboardView.vue')
    },
    {
      path: '/Dashboard/users',
      name: 'users',
      component: () => import('../views/DashboardView.vue')
    },
    {
      path: '/Dashboard/agences',
      name: 'agencies',
      component: () => import('../views/DashboardView.vue')
    },

  ]
})

// router.beforeEach((to, from, next) => {
  // if (userStore.user.role !== 1 ) {

  //   next({ path: '/' });
  // } else {
  //   next();
  // }


  // firebase.auth().onAuthStateChanged(function(user) {
  //   if (to.matched.some(record => record.meta.requiresAuth)) {
  //     if (!user) {
  //       next({ path: '/login' });
  //     } else {
  //       next();
  //     }
  //
  //   } else {

    // if (to.matched.some(record => record.meta.hideForAuth)) {
    //   if (user) {
    //     next({ path: '/dashboard' });
    //   } else {
    //     next();
    //   }
    // } else {
    //   next();
    // }
  // });
// });

export default router
