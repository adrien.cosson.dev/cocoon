import { defineStore } from 'pinia'
import { useLocalStorage } from '@vueuse/core'
import AuthService from "../services/auth.service";

export const useUserStore = defineStore({
  id: 'user',
  state: () => ({
    user: useLocalStorage('user', {
      id: null,
      username: null,
      email: null,
      role: null,
    }
    ),
    token: useLocalStorage('token', null),
    status: null

  }),
  getters: {
    isUserLoggedIn: (state) => {
      console.log(state.token);
      if (state.token !== false && state.token !== 'false' && state.token !== undefined && state.token !== null && state.user.username !== null) {
        return true
      } else {
        return false
      }
    }
  },
  actions: {
    logout() {
      this.token = null
      localStorage.removeItem('token')
      this.user = {
        username: null,
        email: null,
        role: null,
      }
      this.status = null
    },
    async login(user) {
      const response = await AuthService.login(user)
      this.status = "loading"
      if (response === 401) {

        this.user = {
          username: null,
          email: null,
          role: null,
        }
        this.token = undefined
        return false
      }
      if (response.token !== null) {
        this.user = response.user
        this.token = response.token
        this.status = 'connected'
        return true
      }
    },
    async register(user) {
      const response = await AuthService.register(user)
      // console.log(response);
      if (response.status === 201) {
        return response.data
      } else if (response.status === 200) {
        return response.data
      }
    },
  }
})
