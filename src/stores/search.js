import { defineStore } from 'pinia'
import { useLocalStorage } from '@vueuse/core'
import AuthService from "../services/auth.service";

export const useSearchStore = defineStore({
  id: 'search',
  state: () => ({
    search: useLocalStorage('search',
{
            city: null,
            minPrice: null,
            maxPrice: null,
          }
    ),

  }),
  getters: {

  },
  actions: {

  }
})
