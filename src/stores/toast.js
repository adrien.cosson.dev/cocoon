import { defineStore } from 'pinia'
import { useLocalStorage } from '@vueuse/core'
import AuthService from "../services/auth.service";

export const useToastStore = defineStore({
  id: 'toast',
  state: () => ({
    toast: useLocalStorage('toasts', [
    ]
    ),
    // token: useLocalStorage('token', localStorage.getItem('token')),
    // token: useLocalStorage('token', null),

  }),
  getters: {
    // doubleCount: (state) => state.counter * 2
    getActiveToast: (state) => {
      console.log(Date.now());
      console.log(Date.now() - 10);
      return state.toast.filter(function (toast) {
        // console.log(toast.time);
        // console.log("coucou");
        return toast.time > (Date.now() - 300000)
      })
    },
  },
  actions: {
    newToast(toast) {
      console.log(toast);
      this.toast.push(toast)
    },
    deleteToast(index) {
      this.toast.splice(index, 1)
    },
  }
})
