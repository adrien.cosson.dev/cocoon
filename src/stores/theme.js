import { defineStore } from 'pinia'
import { useLocalStorage } from '@vueuse/core'

export const useThemeStore = defineStore({
  id: 'theme',
  state: () => ({
    // counter: 0,
    dark: useLocalStorage('themeStore.dark',true),
    sidebarOpened : useLocalStorage('themeStore.sidebarOpened', true),
    leftHanded : useLocalStorage('themeStore.leftHanded', false),
  }),
  // getters: {
  //   doubleCount: (state) => state.counter * 2
  // },
  // actions: {
  //   increment() {
  //     this.counter++
  //   }
  // }
})
