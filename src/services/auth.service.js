import axios from 'axios';
const API_URL = 'http://0.0.0.0/api/';

class AuthService {
    login(user) {
        const headers = {
            "Content-Type": "application/json",
        };

            return axios
                .post(API_URL + 'login', {
                    username: user.username,
                    password: user.password
                }, {headers})
                .then(response => {
                    if (response.data.token) {
                        return response.data;
                    } else {
                        return {success: false}
                    }
                })
                .catch(function (error) {
                    return error.response.status
                })


    }

    register(user) {
        return axios.post(API_URL + 'register', {
            username: user.username,
            email: user.email,
            password: user.password,
            password_c: user.passwordConfirmation
        });
    }
}
export default new AuthService();
