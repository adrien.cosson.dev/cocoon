import axios from 'axios';
import authHeader from './auth-header';
const API_URL = 'http://0.0.0.0/api/';

class AppimmoService {
    get(url) {
        return axios.get(API_URL + url,
            {
            headers: authHeader()
        }
        )
    }
    store(data, url) {
        // alert(authHeader())
        return axios.post(API_URL + url, data, {
            headers: authHeader()
        })
    }

    update(data, url) {
        return axios.patch(API_URL + url, data, {
            headers: authHeader()
        })
    }

    destroy(url) {
        return axios.delete(API_URL + url, {
            headers: authHeader()
        })
    }
}
export default new AppimmoService();
